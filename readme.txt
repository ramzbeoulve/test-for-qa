== Запуск тестов ==
1. Команда для запуска смоука, перед выполнение перейти в папку tests (cd test):
        Python -m pytest -m smoke --alluredir="../reports" test_main_page.py

2. Команда для запуска всех тестов, перед выполнение перейти в папку tests (cd test):
        Python -m pytest --alluredir="../reports" test_main_page.py

== Сбор отчетов ==
3. Установить расширение Allure и прописать путь до папки reports для генерации Allure-отчетов в дженкинсе

4. Для сбора отчета локально нужно запустить батник из папки allure/bin/allure.bat с указанием папки репортов,
например из корня проекта будет выглядить так:
        allure\bin\allure.bat serve “reports”

== Мануальный ЧЛ ==
https://docs.google.com/spreadsheets/d/1dPdPvSoa4yVbOpyrseEG7ffMMUJv4Tw0EdYeidsfh-0/edit?usp=sharing