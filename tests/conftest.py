import pytest
import glob
import os
from selenium import webdriver
from configs.config_manager import ConfigManager

def pytest_addoption(parser):
    parser.addoption('--browser_name', action='store', default="chrome",
                     help="Choose browser: chrome or firefox")


@pytest.fixture(scope="function")
def driver():
    config = ConfigManager.get_config()
    capabilities = {
        "browserName": "chrome",
        "version": "80.0",
        "enableVNC": True,
        "enableVideo": False
    }
    driver = webdriver.Remote(
        command_executor=os.environ.get('SELENOID'),
        desired_capabilities=capabilities)
    driver.get(config.page_url)
    yield driver
    driver.quit()


@pytest.fixture(scope="session")
def clear_allure_reports():
    file_list = glob.glob('../reports/*')
    if os.path.exists('../reports'):
        for f in file_list:
            if os.path.isfile(f):
                os.remove(f)
