import json
import pytest
import allure
from pages.main_page import MainPage


class TestTodosPage:
    @pytest.mark.smoke
    @pytest.mark.regression
    @allure.severity(allure.severity_level.BLOCKER)
    @allure.title('Test page is available')
    def test_page_is_available(self, driver, clear_allure_reports):
        assert driver.title == 'React • TodoMVC'

    @pytest.mark.smoke
    @pytest.mark.regression
    @allure.severity(allure.severity_level.BLOCKER)
    @allure.title('Create TODO')
    def test_create_todo(self, driver, clear_allure_reports):
        main_page = MainPage(driver)
        assert not main_page.footer_is_visible()
        assert not main_page.todo_section_is_visible()
        assert not main_page.mark_all_is_visible()
        assert main_page.input_is_in_focus()

        main_page.create_todo("New TODO")

        assert main_page.todo_is_visible("New TODO")
        assert main_page.todo_count() == 1
        assert main_page.footer_is_visible()
        assert main_page.todo_section_is_visible()
        assert main_page.mark_all_is_visible()

    @pytest.mark.smoke
    @pytest.mark.regression
    @allure.severity(allure.severity_level.CRITICAL)
    @allure.title('Delete TODO')
    def test_delete_todo(self, driver, clear_allure_reports):
        main_page = MainPage(driver)
        main_page.create_todo("first")
        main_page.create_todo("second")

        assert main_page.todo_is_visible("first")
        assert main_page.todo_is_visible("second")

        main_page.delete_todo("first")
        assert not main_page.todo_is_visible("first")
        assert main_page.todo_is_visible("second")

    @pytest.mark.regression
    @allure.severity(allure.severity_level.NORMAL)
    @allure.title('Test Placeholder')
    def test_placeholder(self, driver, clear_allure_reports):
        main_page = MainPage(driver)
        assert main_page.placeholder_text() == "What needs to be done?"

    @pytest.mark.smoke
    @pytest.mark.regression
    @allure.severity(allure.severity_level.CRITICAL)
    @allure.title('Toggle TODO as completed')
    def test_toggle_todo_completed(self, driver, clear_allure_reports):
        main_page = MainPage(driver)
        main_page.create_todo("first")
        main_page.create_todo("second")

        main_page.toggle_todo("second")
        assert main_page.todo_is_active("first")
        assert not main_page.todo_is_active("second")

    @pytest.mark.smoke
    @pytest.mark.regression
    @allure.severity(allure.severity_level.CRITICAL)
    @allure.title('Toggle TODO as active')
    def test_toggle_todo_active(self, driver, clear_allure_reports):
        main_page = MainPage(driver)
        main_page.create_todo("first")
        main_page.create_todo("second")

        main_page.toggle_todo("second")
        assert main_page.todo_is_active("first")
        assert not main_page.todo_is_active("second")

        main_page.toggle_todo("second")
        assert main_page.todo_is_active("first")
        assert main_page.todo_is_active("second")

    @pytest.mark.smoke
    @pytest.mark.regression
    @allure.severity(allure.severity_level.CRITICAL)
    @allure.title('Edit TODO')
    def test_edit_todo(self, driver, clear_allure_reports):
        main_page = MainPage(driver)
        main_page.create_todo("first value")
        main_page.edit_todo("first value", "new value")
        assert main_page.todo_is_visible("new value")
        assert not main_page.todo_is_visible("first value")

    @pytest.mark.regression
    @allure.severity(allure.severity_level.NORMAL)
    @allure.title('Edit TODO and cancel. Test ESCAPE')
    def test_cancel_edit_todo(self, driver, clear_allure_reports):
        main_page = MainPage(driver)
        main_page.create_todo("first value")
        main_page.edit_todo_and_cancel("first value", "new value")
        assert not main_page.todo_is_visible("new value")
        assert main_page.todo_is_visible("first value")

    @pytest.mark.regression
    @allure.severity(allure.severity_level.CRITICAL)
    @allure.title('Delete TODO with new empty name')
    def test_delete_with_blank_value(self, driver, clear_allure_reports):
        main_page = MainPage(driver)
        main_page.create_todo("first")
        main_page.create_todo("second")
        main_page.edit_todo("first", " ")
        assert not main_page.todo_is_visible("first")
        assert main_page.todo_is_visible("second")
        assert main_page.todo_count() == 1

    @pytest.mark.smoke
    @pytest.mark.regression
    @allure.severity(allure.severity_level.NORMAL)
    @allure.title('Can\'t create empty TODO')
    def test_cant_create_empty_todo(self, driver, clear_allure_reports):
        main_page = MainPage(driver)
        main_page.create_todo(" ")
        assert main_page.todo_count() == 0

    @pytest.mark.regression
    @allure.severity(allure.severity_level.NORMAL)
    @allure.title('Test trim spaces')
    def test_cant_create_empty_todo(self, driver, clear_allure_reports):
        main_page = MainPage(driver)
        main_page.create_todo("   HELLO   ")
        assert not main_page.todo_is_visible("   HELLO   ")
        assert main_page.todo_is_visible("HELLO")

    @pytest.mark.smoke
    @pytest.mark.regression
    @allure.severity(allure.severity_level.NORMAL)
    @allure.title('Mark all')
    def test_mark_all(self, driver, clear_allure_reports):
        main_page = MainPage(driver)
        main_page.create_todo("first")
        main_page.create_todo("second")
        assert main_page.todo_is_active("first")
        assert main_page.todo_is_active("second")

        main_page.toggle_mark_all()
        assert not main_page.todo_is_active("first")
        assert not main_page.todo_is_active("second")

    @pytest.mark.smoke
    @pytest.mark.regression
    @allure.severity(allure.severity_level.NORMAL)
    @allure.title('Unmark all')
    def test_unmark_all(self, driver, clear_allure_reports):
        main_page = MainPage(driver)
        main_page.create_todo("first")
        main_page.create_todo("second")
        assert main_page.todo_is_active("first")
        assert main_page.todo_is_active("second")

        main_page.toggle_mark_all()
        assert not main_page.todo_is_active("first")
        assert not main_page.todo_is_active("second")

        main_page.toggle_mark_all()
        assert main_page.todo_is_active("first")
        assert main_page.todo_is_active("second")

    @pytest.mark.regression
    @allure.severity(allure.severity_level.NORMAL)
    @allure.title('Mark part and mark all')
    def test_unmark_all(self, driver, clear_allure_reports):
        main_page = MainPage(driver)
        main_page.create_todo("first")
        main_page.create_todo("second")
        main_page.create_todo("third")

        main_page.toggle_todo("first")
        assert not main_page.todo_is_active("first")
        assert main_page.todo_is_active("second")
        assert main_page.todo_is_active("third")

        main_page.toggle_mark_all()
        assert not main_page.todo_is_active("first")
        assert not main_page.todo_is_active("second")
        assert not main_page.todo_is_active("third")

    @pytest.mark.regression
    @allure.severity(allure.severity_level.NORMAL)
    @allure.title('Test counter')
    def test_active_todo_counter(self, driver, clear_allure_reports):
        main_page = MainPage(driver)
        main_page.create_todo("first")
        main_page.create_todo("second")
        assert main_page.active_todo_count_text() == "2 items left"

        main_page.toggle_todo("first")
        assert main_page.active_todo_count_text() == "1 item left"

        main_page.toggle_todo("second")
        assert main_page.active_todo_count_text() == "0 items left"

        main_page.toggle_todo("second")
        assert main_page.active_todo_count_text() == "1 item left"

        main_page.delete_todo("second")
        assert main_page.active_todo_count_text() == "0 items left"

    @pytest.mark.smoke
    @pytest.mark.regression
    @allure.severity(allure.severity_level.NORMAL)
    @allure.title('Test active tab')
    def test_active_tab(self, driver, clear_allure_reports):
        main_page = MainPage(driver)
        main_page.create_todo("first")
        main_page.create_todo("second")
        main_page.toggle_todo("first")
        main_page.show_active()
        assert not main_page.todo_is_visible("first")
        assert main_page.todo_is_active("second")

    @pytest.mark.smoke
    @pytest.mark.regression
    @allure.severity(allure.severity_level.NORMAL)
    @allure.title('Test completed tab')
    def test_completed_tab(self, driver, clear_allure_reports):
        main_page = MainPage(driver)
        main_page.create_todo("first")
        main_page.create_todo("second")
        main_page.toggle_todo("first")
        main_page.show_completed()
        assert not main_page.todo_is_active("first")
        assert not main_page.todo_is_visible("second")

    @pytest.mark.smoke
    @pytest.mark.regression
    @allure.severity(allure.severity_level.NORMAL)
    @allure.title('Test All tab')
    def test_all_tab(self, driver, clear_allure_reports):
        main_page = MainPage(driver)
        main_page.create_todo("first")
        main_page.create_todo("second")
        main_page.toggle_todo("first")
        main_page.show_active()
        assert not main_page.todo_is_visible("first")
        assert main_page.todo_is_active("second")

        main_page.show_all()
        assert not main_page.todo_is_active("first")
        assert main_page.todo_is_active("second")

    @pytest.mark.regression
    @allure.severity(allure.severity_level.NORMAL)
    @allure.title('Clear Completed is invisible when zero TODOs completed')
    def test_clear_completed_is_invisible(self, driver, clear_allure_reports):
        main_page = MainPage(driver)
        main_page.create_todo("first")
        main_page.create_todo("second")
        assert not main_page.clear_completed_is_visible()

        main_page.toggle_todo("second")
        assert main_page.clear_completed_is_visible()

        main_page.delete_todo("second")
        assert not main_page.clear_completed_is_visible()

    @pytest.mark.smoke
    @pytest.mark.regression
    @allure.severity(allure.severity_level.NORMAL)
    @allure.title('Clear Completed')
    def test_clear_completed(self, driver, clear_allure_reports):
        main_page = MainPage(driver)
        main_page.create_todo("first")
        main_page.create_todo("second")
        main_page.toggle_todo("second")
        main_page.clear_completed()
        assert not main_page.todo_is_visible("second")
        assert main_page.todo_is_visible("first")

    @pytest.mark.regression
    @allure.severity(allure.severity_level.NORMAL)
    @allure.title('Mark all resets after clear Completed')
    def test_mark_all_reset(self, driver, clear_allure_reports):
        main_page = MainPage(driver)
        main_page.create_todo("first")
        main_page.create_todo("second")
        main_page.toggle_mark_all()

        assert not main_page.todo_is_active("first")
        assert not main_page.todo_is_active("second")

        main_page.clear_completed()
        assert not main_page.todo_is_visible("first")
        assert not main_page.todo_is_visible("second")

        main_page.create_todo("third")
        assert main_page.todo_is_active("third")

    @pytest.mark.regression
    @allure.severity(allure.severity_level.NORMAL)
    @allure.title('Local storage')
    def test_local_storage(self, driver, clear_allure_reports):
        main_page = MainPage(driver)
        main_page.create_todo("first")
        main_page.create_todo("second")
        main_page.toggle_todo("second")
        local_storage = json.loads(driver.execute_script("return window.localStorage.getItem('react-todos')"))
        assert local_storage[0]["title"] == "first" and not local_storage[0]["completed"]
        assert local_storage[1]["title"] == "second" and local_storage[1]["completed"]

        main_page.delete_todo("second")
        new_local_storage = json.loads(driver.execute_script("return window.localStorage.getItem('react-todos')"))
        assert new_local_storage[0]["title"] == "first"
        try:
            var = new_local_storage[1]["title"]
            assert False
        except IndexError:
            assert True

    @pytest.mark.regression
    @allure.severity(allure.severity_level.NORMAL)
    @allure.title('Header is correct')
    def test_header(self, driver, clear_allure_reports):
        main_page = MainPage(driver)
        assert main_page.header_text() == 'todos'

    @pytest.mark.regression
    @allure.severity(allure.severity_level.NORMAL)
    @allure.title('Created by link is correct')
    def test_created_by_link(self, driver, clear_allure_reports):
        main_page = MainPage(driver)
        main_page.follow_created_by_link()
        assert driver.title == 'petehunt (Pete Hunt) · GitHub'

    @pytest.mark.regression
    @allure.severity(allure.severity_level.NORMAL)
    @allure.title('Part of link is correct')
    def test_part_of_link(self, driver, clear_allure_reports):
        main_page = MainPage(driver)
        main_page.follow_part_of_link()
        assert driver.title == 'TodoMVC'
