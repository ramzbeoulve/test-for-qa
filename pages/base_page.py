from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.ui import WebDriverWait


class BasePage:
    def __init__(self, driver):
        self.driver = driver

    def find_element(self, locator, time=3):
        return WebDriverWait(self.driver, time).until(ec.presence_of_element_located(locator),
                                                      message=f"Can't find element by locator {locator}")

    def find_elements(self, locator, time=3):
        return WebDriverWait(self.driver, time).until(ec.presence_of_all_elements_located(locator),
                                                      message=f"Can't find elements by locator {locator}")

    def write_text(self, locator, text):
        self.find_element(locator).send_keys(text)

    def get_element_index_by_text(self, locator, text):
        list_items = self.find_elements(locator)
        for item in list_items:
            if text == item.text:
                return list_items.index(item)
            if item == list_items[len(list_items) - 1]:
                return None

    def click_by_index(self, locator, index):
        elements = self.find_elements(locator)
        elements[index].click()

    def click(self, locator):
        self.find_element(locator).click()

    def get_attribute_by(self, locator, attribute):
        return self.find_element(locator).get_attribute(attribute)

    def locator_is_in_dom(self, locator):
        if not self.driver.find_elements(*locator):
            return False
        else:
            return True

    def get_element_in_focus(self):
        return self.driver.switch_to.active_element
