import allure
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.keys import Keys
from .base_page import BasePage
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains


class MainPageLocators:
    input_field = (By.XPATH, "//*[@class='new-todo']")
    todo_li = (By.XPATH, "//section/ul/li")
    todo_text = (By.XPATH, "//div/label")
    todo_toggle = (By.XPATH, "//input[@class='toggle']")
    todo_destroy = (By.XPATH, "//button[@class='destroy']")
    mark_all = (By.XPATH, "//label[@for='toggle-all']")
    footer = (By.XPATH, "/html/body/section/div/footer")
    section = (By.XPATH, "/html/body/section/div/section")
    counter_number = (By.XPATH, "//span[@class='todo-count']/strong")
    counter_text = (By.XPATH, "//*[@class='todo-count']/span")
    filter_all = (By.XPATH, "//a[@href='#/']")
    filter_active = (By.XPATH, "//a[@href='#/active']")
    filter_completed = (By.XPATH, "//a[@href='#/completed']")
    clear_completed = (By.XPATH, "//button[@class='clear-completed']")
    header = (By.XPATH, "//header/h1")
    created_by = (By.XPATH, "//p[text()='Created by ']/a")
    part_of = (By.XPATH, "//p[text()='Part of ']/a")


class MainPage(BasePage):
    @allure.step('Create TODO')
    def create_todo(self, todo_name):
        self.write_text(MainPageLocators.input_field, todo_name)
        self.write_text(MainPageLocators.input_field, Keys.ENTER)

    @allure.step('Toggle TODO')
    def toggle_todo(self, todo_name):
        index = self.get_element_index_by_text(MainPageLocators.todo_text, todo_name)
        if index is not None:
            self.click_by_index(MainPageLocators.todo_toggle, index)
        else:
            raise AssertionError(f'No element with text: {todo_name}')

    @allure.step('Toggle Mark All')
    def toggle_mark_all(self):
        self.click(MainPageLocators.mark_all)

    @allure.step('Check TODO is visible')
    def todo_is_visible(self, todo_name):
        try:
            if self.get_element_index_by_text(MainPageLocators.todo_text, todo_name) is None:
                return False
            else:
                return True
        except TimeoutException:
            return False

    @allure.step('Check TODO is active')
    def todo_is_active(self, todo_name):
        lis = self.find_elements(MainPageLocators.todo_li)
        labels = self.find_elements(MainPageLocators.todo_text)
        index = self.get_element_index_by_text(MainPageLocators.todo_text, todo_name)
        if index is not None:
            if "completed" in lis[index].get_attribute("class") \
                    and "line-through" in labels[index].value_of_css_property("text-decoration"):
                return False
            else:
                return True
        else:
            raise AssertionError(f'No element with text: {todo_name}')

    @allure.step('Edit TODO')
    def edit_todo(self, old_todo_name, new_todo_name):
        todos = self.find_elements(MainPageLocators.todo_text)
        index = self.get_element_index_by_text(MainPageLocators.todo_text, old_todo_name)
        actions = ActionChains(self.driver)
        if index is not None:
            actions.double_click(todos[index]).key_down(Keys.CONTROL).send_keys('a').key_up(Keys.CONTROL) \
                .send_keys(Keys.BACKSPACE).send_keys(new_todo_name).send_keys(Keys.ENTER).perform()
        else:
            raise AssertionError(f'No element with text: {old_todo_name}')

    @allure.step('Edit TODO and cancel')
    def edit_todo_and_cancel(self, old_todo_name, new_todo_name):
        todos = self.find_elements(MainPageLocators.todo_text)
        index = self.get_element_index_by_text(MainPageLocators.todo_text, old_todo_name)
        actions = ActionChains(self.driver)
        if index is not None:
            actions.double_click(todos[index]).key_down(Keys.CONTROL).send_keys('a').key_up(Keys.CONTROL) \
                .send_keys(Keys.BACKSPACE).send_keys(new_todo_name).send_keys(Keys.ESCAPE).perform()
        else:
            raise AssertionError(f'No element with text: {old_todo_name}')

    @allure.step('Count TODO')
    def todo_count(self):
        try:
            return len(self.find_elements(MainPageLocators.todo_li))
        except TimeoutException:
            return 0

    @allure.step('Get completed TODO counter text')
    def active_todo_count_text(self):
        number = self.find_element(MainPageLocators.counter_number).text
        elements = self.find_elements(MainPageLocators.counter_text)
        text = ''
        for i in elements:
            if i != elements[len(elements) - 1]:
                text += i.text + " "
            else:
                text += i.text
        return number + text

    @allure.step('Delete TODO')
    def delete_todo(self, todo_name):
        todos = self.find_elements(MainPageLocators.todo_li)
        destroys = self.find_elements(MainPageLocators.todo_destroy)
        index = self.get_element_index_by_text(MainPageLocators.todo_text, todo_name)
        actions = ActionChains(self.driver)
        if index is not None:
            actions.move_to_element(todos[index]).move_to_element(destroys[index]) \
                .click(destroys[index]).perform()
        else:
            raise AssertionError(f'No element with text: {todo_name}')

    @allure.step('Input field is in focus')
    def input_is_in_focus(self):
        input_field = self.find_element(MainPageLocators.input_field)
        in_focus = self.get_element_in_focus()
        return input_field == in_focus

    @allure.step('Click <Active>')
    def show_active(self):
        self.click(MainPageLocators.filter_active)

    @allure.step('Click <Completed>')
    def show_completed(self):
        self.click(MainPageLocators.filter_completed)

    @allure.step('Click <All>')
    def show_all(self):
        self.click(MainPageLocators.filter_all)

    @allure.step('Click <Clear Completed>')
    def clear_completed(self):
        self.click(MainPageLocators.clear_completed)

    @allure.step('Get placeholder text')
    def placeholder_text(self):
        return self.get_attribute_by(MainPageLocators.input_field, "placeholder")

    @allure.step('Clear Completed check visibility')
    def clear_completed_is_visible(self):
        return self.locator_is_in_dom(MainPageLocators.clear_completed)

    @allure.step('Footer check visibility')
    def footer_is_visible(self):
        return self.locator_is_in_dom(MainPageLocators.footer)

    @allure.step('TODO section check visibility')
    def todo_section_is_visible(self):
        return self.locator_is_in_dom(MainPageLocators.section)

    @allure.step('Mark all check visibility')
    def mark_all_is_visible(self):
        return self.locator_is_in_dom(MainPageLocators.mark_all)

    @allure.step('Get header text')
    def header_text(self):
        return self.find_element(MainPageLocators.header).text

    @allure.step('Open Created by page')
    def follow_created_by_link(self):
        self.click(MainPageLocators.created_by)

    @allure.step('Open Part of page')
    def follow_part_of_link(self):
        self.click(MainPageLocators.part_of)
